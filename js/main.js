
$(".btn_modal").fancybox({
    'padding'    : 0
});


(function() {

    $('.header-bar__search_toggle').on('click touchstart', function(e){
        e.preventDefault();
        $(this).closest('.header-bar__search').toggleClass('open');
    });

}());


(function() {

    $('.nav-bar__button').on('click touchstart', function(e){
        e.preventDefault();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        }
        else {
            $(this).closest('.nav-bar__menu').find('.nav-bar__button').removeClass('active');
            $(this).addClass('active');
        }
    });

}());


(function() {

    $('.nav_toggle').on('click', function(e){
        e.preventDefault();
        $('.nav-second').removeClass('open');
        $('.nav').toggleClass('open');
        $(this).toggleClass('open');
    });

    $('.nav .dropdown').on('click', function(e){
        e.preventDefault();
        $(this).closest('li').find('> ul').slideToggle('fast');
        $(this).closest('li').toggleClass('open');
    });


    $('.second_toggle').on('click', function(e){
        e.preventDefault();
        $('.nav').removeClass('open');
        $('.nav_toggle').removeClass('open');
        $('.nav-second').toggleClass('open');
    });



    $('.nav-second__favorite').on('click', function(e){
        e.preventDefault();
        $(this).toggleClass('added');
    });

}());

$('.raty').raty({
    number: 5,
    starHalf      : 'star-half-png',
    starOff       : 'star-off-png',
    starOn        : 'star-on-png',
    cancelOff     : 'cancel-off-png',
    cancelOn      : 'cancel-on-png',
    score: function() {
        return $(this).attr('data-score');
    },
    readOnly: function() {
        return $(this).attr('data-readOnly');
    },
});


var pointsSlider = new Swiper ('.points__slider', {
    loop: true,
    navigation: {
        nextEl: '.points__nav_next',
        prevEl: '.points__nav_prev',
    }
});


var tourContent = new Swiper ('.tour__content_slider', {
    loop: true,
    navigation: {
        nextEl: '.tour__content_next',
        prevEl: '.tour__content_prev',
    }
});


var gallery = new Swiper('.tour___gallery', {
    slidesPerView: 'auto',
    spaceBetween: 10,
});

(function() {
    $('.players-status').on('click touchstart', function(e){
        e.preventDefault();
        $(this).toggleClass('players-status__selected');
    });
}());

(function() {

    $('.point__group_title').on('click', function(e){
        e.preventDefault();
        var box = $(this).closest('.point__group');
        box.toggleClass('active');
        box.find('.point__group_content').slideToggle('fast');

    });
}());

(function() {

    $('.auth_close').on('click', function(e){
        e.preventDefault();
        $(this).closest('.auth').removeClass('open');
    });
}());


var promo = new Swiper ('.promo__slider', {
    loop: true,
    pagination: {
        el: '.swiper-pagination',
        clickable: true
    }
});

var docs = new Swiper ('.doc__gallery_slider', {
    loop: true,
    spaceBetween: 20
});



pickmeup.defaults.locales['ru'] = {
    days: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
    daysShort: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    daysMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
    monthsShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн', 'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек']
};

pickmeup('.calendar', {
    flat : true,
    mode : 'multiple',
    locale: 'ru',
    prev: '‹',
    next: '›',
    class_name: 'ddays'
});

