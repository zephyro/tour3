<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">
            <div class="grid">

                <?php include('inc/header.inc.php') ?>

                <?php include('inc/nav.inc.php') ?>

                <div class="main main_single">
                    <div class="profile-new">
                        <div class="profile-new__title">Профиль</div>
                        <form class="form">
                            <div class="form-group">
                                <div class="profile-new__status">
                                    <div class="profile-new__status_label">Статус</div>
                                    <label class="profile-new__status_value">
                                        <input type="radio" name="status" value="">
                                        <span>Турист</span>
                                    </label>
                                    <label class="profile-new__status_value">
                                        <input type="radio" name="status" value="">
                                        <span>Гид</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="login" placeholder="Логин / Электронная почта">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" name="password" placeholder="Пароль">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" placeholder="Имя">
                            </div>

                            <div class="profile-new__upload">
                                <label class="file-photo">
                                    <input type="file" name="photo">
                                    <div class="file-photo__image">
                                        <svg class="ico_svg" viewBox="0 0 90 90" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#profile__avatar_men" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="file-photo__text">
                                        Загрузить<br/>фото<br/>профиля
                                    </div>
                                    <div class="file-photo__image">
                                        <svg class="ico_svg" viewBox="0 0 90 90" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#profile__avatar_woman" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </label>
                                <label class="file-image">
                                    <input type="file" name="upload_image">
                                    <span>Загрузить<br/> обложку <br/> профиля</span>
                                </label>
                            </div>

                            <div class="profile-new__subtitle">Контактные данные</div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="email" placeholder="Электронная почта">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="text" name="phone" placeholder="Телефон">
                            </div>
                            <div class="form-group">
                                <div class="form-social">
                                    <a href="#" class="form-social__item">
                                        <svg class="ico_svg" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="form-social__item">
                                        <svg class="ico_svg" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                    <a href="#" class="form-social__item">
                                        <svg class="ico_svg" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                            <div class="profile-new__button">
                                <div class="text_center mb_10">
                                    <button type="submit" class="btn">Сохранить профиль</button>
                                </div>
                                <div class="text_center">
                                    <button type="submit" class="btn btn_border">Создать новый тур</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php include('inc/footer.inc.php') ?>

            </div>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
