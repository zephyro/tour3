<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <div class="grid">

                <?php include('inc/header.inc.php') ?>

                <?php include('inc/nav.inc.php') ?>

                <section class="main">

                </section>

                <?php include('inc/footer.inc.php') ?>

            </div>
        </div>

        <div class="auth open" id="auth">
            <div class="auth__wrap">
                <div class="auth__box">
                    <div class="auth__title">Войти</div>
                    <div class="mb_10">
                        <form>
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" placeholder="Логин / Почта">
                            </div>
                            <div class="form-group  mb_30">
                                <input type="text" class="form-control" name="password" placeholder="Пароль">
                            </div>
                            <div class="text_center mb_20">
                                <button type="submit" class="btn btn_blue btn_fix">Войти на сайт</button>
                            </div>
                            <div class="text_center">
                                <a href="#" class="btn-link">Забыли пароль?</a>
                            </div>
                        </form>
                    </div>
                    <div class="auth__subtitle">или</div>
                    <div class="social social_auth social_center">
                        <a href="#" class="social__item">
                            <svg class="ico_svg" viewBox="0 0 47.361 47.361" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#ic__fb" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </a>
                        <a href="#" class="social__item">
                            <svg class="ico_svg" viewBox="0 0 47.361 47.361" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#ic__g" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </a>
                        <a href="#" class="social__item">
                            <svg class="ico_svg" viewBox="0 0 47.361 47.361" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#ic__tw" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="text_center">
                    <button type="button" class="btn_icon auth_close">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 28.011 28.011" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                </div>
            </div>
        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>

