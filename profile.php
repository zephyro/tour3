<!doctype html>
<html class="no-js" lang="">

<head>
    <?php include('inc/head.inc.php') ?>
</head>

<body>

<div class="page">
    <div class="grid">

        <?php include('inc/header.inc.php') ?>

        <?php include('inc/nav.inc.php') ?>

        <div class="page-top page-top_yellow">
            <a href="#">
                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__arrow_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </a>
            <span>Профиль Имя</span>
        </div>

        <div class="profile-header">
            <div class="profile-header__bg" style="background-image: url('images/slide_01.jpg');"></div>
            <div class="profile-header__photo">
                <div class="profile-header__photo_wrap">
                    <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#nav__profile" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </div>
            </div>
        </div>

        <div class="main main_single">
            <div class="profile">

                <div class="profile__info">
                    <div class="profile__info_name">
                        <strong>Имя</strong>
                        <span>Статус гид</span>
                    </div>
                    <div class="profile__info_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae nunc at odio varius gravida. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. </div>
                </div>

                <div class="profile__contact">
                    <div class="profile__contact_title">Контактные данные</div>
                    <div class="profile__contact_main">
                        <div class="profile__contact_left">
                            <a class="profile__contact_email" href="mailto:name@gmail.com">name@gmail.com</a>
                            <a class="profile__contact_phone" href="tel:">+00 (000) 000-00-00</a>
                            <div class="social">
                                <a href="#" class="social__item">
                                    <svg class="ico_svg" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__telegram" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </a>
                                <a href="#" class="social__item">
                                    <svg class="ico_svg" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__viber" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </a>
                                <a href="#" class="social__item">
                                    <svg class="ico_svg" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__whatsapp" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        <div class="profile__contact_right">
                            <a href="#" class="profile__contact_message">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__email_new" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                                <span>Написать гиду</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="profile__links">
                    <a href="#" class="profile__links_item">
                        <i>
                            <svg class="img_fluid" viewBox="0 0 235 235" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#profile__select" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Избранное</span>
                    </a>
                    <a href="#" class="profile__links_item">
                        <i>
                            <svg class="img_fluid" viewBox="0 0 235 235" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#profile__maxi" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Список<br/> туров</span>
                    </a>
                    <a href="#" class="profile__links_item">
                        <i>
                            <svg class="img_fluid" viewBox="0 0 235 235" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#profile__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Завершенные<br/> туры</span>
                    </a>
                    <a href="#" class="profile__links_item">
                        <i>
                            <svg class="img_fluid" viewBox="0 0 235 235" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#profile__calendar" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                         <span>Календарь<br/> туров</span>
                    </a>
                    <a href="#" class="profile__links_item">
                        <i>
                            <svg class="img_fluid" viewBox="0 0 235 235" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#profile__lang" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Страны<br/> Языки</span>
                    </a>
                    <a href="#" class="profile__links_item">
                        <i>
                            <svg class="img_fluid" viewBox="0 0 235 235" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#profile__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <span>Отзывы</span>
                    </a>
                </div>

            </div>
        </div>

        <?php include('inc/footer.inc.php') ?>

    </div>

</div>

<?php include('inc/scripts.inc.php') ?>

</body>
</html>