<!doctype html>
<html class="no-js" lang="">

    <head>
        <?php include('inc/head.inc.php') ?>
    </head>

    <body>

        <div class="page">

            <div class="grid">

                <?php include('inc/header.inc.php') ?>

                <?php include('inc/nav.inc.php') ?>

                <div class="promo">
                    <div class="promo__slider swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('images/slide_01.jpg');"></div>
                            </div>
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('images/slide_02.jpg');"></div>
                            </div>
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('images/slide_03.jpg');"></div>
                            </div>
                            <div class="swiper-slide">
                                <div class="promo__item" style="background-image: url('images/slide_04.jpg');"></div>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>

                <div class="main">

                    <div class="tour">
                        <div class="tour__elem tour__elem_long">
                            <div class="tour__header">
                                <div class="tour__header_title">Название тура</div>
                                <div class="tour__header_edit">
                                    <a href="#">
                                        <span>Редактировать</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tour__elem">
                            <div class="tour__meta">
                                <div class="tour__meta_item">
                                    <div class="tour__rating">
                                        <div class="tour__rating_star">
                                            <div class="raty" data-score="4.5" data-readOnly="true"></div>
                                        </div>
                                        <div class="tour__rating_value">4.5</div>
                                    </div>
                                    <div class="tour__meta_label">рейтинг</div>
                                </div>
                                <div class="tour__meta_item">
                                    <div class="tour__meta_info">
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__heart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>10</span>
                                    </div>
                                    <div class="tour__meta_label">в избранном</div>
                                </div>
                                <div class="tour__meta_item">
                                    <div class="tour__meta_info">
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__comment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>18</span>
                                    </div>
                                    <div class="tour__meta_label">отзывы</div>
                                </div>
                            </div>
                            <div class="tour__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet leo leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer in risus tincidunt, dictum felis ac, tincidunt ligula. Donec blandit enim sed eros ultrices, eu pulvinar eros rutrum.</div>
                        </div>
                        <div class="tour__elem">
                            <div class="tour-detail">
                                <div class="tour-detail__half">
                                    <div class="tour-detail__content">
                                        <div class="tour-amount">
                                            <div class="tour-amount__summary">
                                                <div class="tour-amount__summary_wrap">
                                                    <strong>3</strong>
                                                    <span>дня</span>
                                                </div>
                                            </div>
                                            <div class="tour-amount__text">Общее количество дней тура</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tour-detail__half">
                                    <div class="tour-amount__info">
                                        <strong>Ближайшие даты</strong>
                                        <strong class="color_blue">Апрель</strong>
                                        <span>04.04 - 06.04</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tour__elem tour__elem_long">
                            <div class="tour__content">

                                <div class="swiper-container tour__content_slider">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 1</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 2</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 3</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 4</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tour__content_nav tour__content_prev">
                                        <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="tour__content_nav tour__content_next">
                                        <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="tour">
                        <div class="tour__elem tour__elem_long">
                            <div class="tour__header">
                                <div class="tour__header_title">Нереально очень и очень длинное название тура</div>
                                <div class="tour__header_edit">
                                    <a href="#">
                                        <span>Редактировать</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tour__elem">
                            <div class="tour__meta">
                                <div class="tour__meta_item">
                                    <div class="tour__rating">
                                        <div class="tour__rating_star">
                                            <div class="raty" data-score="4.5" data-readOnly="true"></div>
                                        </div>
                                        <div class="tour__rating_value">4.5</div>
                                    </div>
                                    <div class="tour__meta_label">рейтинг</div>
                                </div>
                                <div class="tour__meta_item">
                                    <div class="tour__meta_info">
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__heart" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>10</span>
                                    </div>
                                    <div class="tour__meta_label">в избранном</div>
                                </div>
                                <div class="tour__meta_item">
                                    <div class="tour__meta_info">
                                        <i>
                                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                                <use xlink:href="img/sprite_icons.svg#icon__comment" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                            </svg>
                                        </i>
                                        <span>18</span>
                                    </div>
                                    <div class="tour__meta_label">отзывы</div>
                                </div>
                            </div>
                            <div class="tour__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet leo leo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer in risus tincidunt, dictum felis ac, tincidunt ligula. Donec blandit enim sed eros ultrices, eu pulvinar eros rutrum.</div>
                        </div>
                        <div class="tour__elem">
                            <div class="tour-detail">
                                <div class="tour-detail__half">
                                    <div class="tour-detail__content">
                                        <div class="tour-amount">
                                            <div class="tour-amount__summary">
                                                <div class="tour-amount__summary_wrap">
                                                    <strong>3</strong>
                                                    <span>дня</span>
                                                </div>
                                            </div>
                                            <div class="tour-amount__text">Общее количество дней тура</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tour-detail__half">
                                    <div class="tour-amount__info">
                                        <strong>Ближайшие даты</strong>
                                        <strong class="color_blue">Апрель</strong>
                                        <span>04.04 - 06.04</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tour__elem tour__elem_long">
                            <div class="tour__content">

                                <div class="swiper-container tour__content_slider">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 1</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 2</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 3</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="tour__content_header">День 4</div>
                                            <div class="tour__content_main">
                                                <div class="tour__content_map" style="background-image: url('images/map.png');"></div>
                                                <div class="tour__content_image"></div>
                                                <div class="tour__content_image"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tour__content_nav tour__content_prev">
                                        <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="tour__content_nav tour__content_next">
                                        <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <div class="infobox">
                    <div style="min-height: 400px;"></div>
                </div>

                <?php include('inc/footer.inc.php') ?>

            </div>

        </div>

        <?php include('inc/scripts.inc.php') ?>

    </body>
</html>
