 <nav class="nav-second">
     <div class="nav-second__row">

         <div class="nav-second__col">
             <a href="#" class="nav-second__elem nav-second__profile active">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__profile" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__profile_content">
                     <div class="nav-second__profile_text">
                         <strong>Helen</strong>
                         <span>профиль</span>
                     </div>
                 </div>
             </a>
         </div>
         <div class="nav-second__col">
             <a href="#" class="nav-second__elem nav-second__message active">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__message" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <span class="nav-second__elem_amount">20</span>
                 <div class="nav-second__elem_text"><span>Мои собщения</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a href="#" class="nav-second__elem nav-second__invitation active">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__invitation" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <span class="nav-second__elem_amount">8</span>
                 <div class="nav-second__elem_text"><span>пришло приглашение</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a class="nav-second__item" href="#">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__mail_new" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__item_text"><span>Написать<br/> гиду</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a class="nav-second__item" href="#">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__comments" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__item_text"><span>Отзывы</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a class="nav-second__item" href="#">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__phone" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__item_text"><span>Позвонить<br/> гиду</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a class="nav-second__item nav-second__favorite" href="#">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__favorite" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__favorite_add"></div>
                 <div class="nav-second__item_text"><span>В избранном</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a class="nav-second__item" href="#">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__tour" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__item_text"><span>Создать<br/>тур</span></div>
             </a>
         </div>
         <div class="nav-second__col">
             <a class="nav-second__item nav-second__favorite" href="#">
                 <svg class="ico_svg" viewBox="0 0 145 145" xmlns="http://www.w3.org/2000/svg">
                     <use xlink:href="img/sprite_icons.svg#nav__calendar" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                 </svg>
                 <div class="nav-second__item_text"><span>Календарь</span></div>
             </a>
         </div>
     </div>
</nav>

