<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-3.4.1.min.js"><\/script>')</script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script src="https://unpkg.com/swiper/js/swiper.min.js"></script>
<script src="js/vendor/svg4everybody.legacy.min.js"></script>
<script src="js/vendor/pickmeup/pickmeup.min.js"></script>
<script src="js/vendor/raty.js/jquery.raty.js"></script>
<script src="js/main.js"></script>