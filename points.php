<!doctype html>
<html class="no-js" lang="">

<head>
    <?php include('inc/head.inc.php') ?>
</head>

<body>

<div class="page">
    <div class="grid">

        <header class="header">
            <div class="header__wrap">

                <div class="header__top">
                    <button class="header__toggle nav_toggle">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 46 30" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_toggle" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <a class="header__logo" href="/">
                        <img src="img/logo.svg" class="img_fluid" about="">
                    </a>
                    <div class="header__content">

                        <nav class="nav">
                            <div class="nav__wrap">
                                <ul class="nav__content">
                                    <li><a href="#"><span>Правила</span></a></li>
                                    <li class="dropdown">
                                        <a href="#">
                                            <span>Условия</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                        <ul>
                                            <li><a href="#"><span>Статус</span></a></li>
                                            <li><a href="#"><span>Принятие решения</span></a></li>
                                            <li><a href="#"><span>Еще раздел</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><span>Поддержка</span></a></li>
                                    <li><a href="#"><span>Карта сайта</span></a></li>
                                    <li><a href="#"><span>Доверие и безопасность</span></a></li>
                                    <li class="dropdown">
                                        <a href="#">
                                            <span>Поддержка</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                        <ul>
                                            <li><a href="#"><span>Статус</span></a></li>
                                            <li><a href="#"><span>Принятие решения</span></a></li>
                                            <li><a href="#"><span>Еще раздел</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                    </div>
                    <button class="header__nav second_toggle alert_one alert_two alert_three">
                        <i class="header__nav_base">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <i class="header__nav_one">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second_one" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <i class="header__nav_two">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second_two" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <i class="header__nav_three">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second_three" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                </div>

                <div class="header-bar">
                    <div class="header-bar__content">
                        <nav class="header-bar__menu">
                            <a class="header-bar__button active" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_map" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <a class="header-bar__button" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_list" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <a class="header-bar__button" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_user" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <a class="header-bar__button" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_doc" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                        </nav>
                    </div>
                    <div class="header-bar__search">
                        <button class="header-bar__search_toggle">
                            <i>
                                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </button>
                        <div class="header-bar__search_content">
                            <div class="header-bar__search_arrow">
                                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="header-bar__search_input">
                                <input type="text" name="search" value="" placeholder="Введите слово поиска">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </header>

        <?php include('inc/nav.inc.php') ?>

        <div class="page-top">
            <a href="#">
                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__arrow_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </a>
            <span>Список проходов между точками</span>
        </div>

        <div class="main main_single">

            <div class="main_wrap">
                <div class="map-point">
                    <div class="map-point__header">
                        <div class="map-point__header_nav map-point__header_prev">
                            <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </div>
                        <div class="map-point__header_title"><strong class="color_blue">2 день</strong> <span>3 точки</span></div>
                        <div class="map-point__header_nav map-point__header_next">
                            <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="map-point__main">

                        <div class="map-point__source"></div>

                        <div class="map-point__list">

                            <div class="points">
                                <div class="points__nav points__nav_prev">
                                    <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>
                                <div class="points__nav points__nav_next">
                                    <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </div>

                                <div class="points__slider swiper-container">
                                    <div class="swiper-wrapper">
                                        <div class="swiper-slide">
                                            <div class="points__header">Первая точка</div>
                                            <div class="points__content">

                                                <div class="point-box">
                                                    <div class="point-box__header">
                                                        <div class="point-box__place">
                                                            <i>
                                                                <img src="img/location.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Точное положение первой точки</span>
                                                        </div>
                                                    </div>
                                                    <div class="point-box__content">
                                                        <p>Nulla sollicitudin lectus quis dui pharetra vestibulum non vitae nisl. In a rhoncus arcu. Cras eget cursus diam. Praesent faucibus urna sit amet elementum dignissim. Nulla sem metus, tempus in imperdiet at, tempor a arcu. </p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="points__header">Вторая точка</div>
                                            <div class="points__content">

                                                <div class="point-box">
                                                    <div class="point-box__header">
                                                        <div class="point-box__place">
                                                            <i>
                                                                <img src="img/location.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Точное положение первой точки</span>
                                                        </div>
                                                    </div>
                                                    <div class="point-box__content">
                                                        <p>Nulla sollicitudin lectus quis dui pharetra vestibulum non vitae nisl. In a rhoncus arcu. Cras eget cursus diam. Praesent faucibus urna sit amet elementum dignissim. Nulla sem metus, tempus in imperdiet at, tempor a arcu. </p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="swiper-slide">
                                            <div class="points__header">Третья точка</div>
                                            <div class="points__content">

                                                <div class="point-box">
                                                    <div class="point-box__header">
                                                        <div class="point-box__place">
                                                            <i>
                                                                <img src="img/location.svg" class="img_fluid" alt="">
                                                            </i>
                                                            <span>Точное положение первой точки</span>
                                                        </div>
                                                    </div>
                                                    <div class="point-box__content">
                                                        <p>Nulla sollicitudin lectus quis dui pharetra vestibulum non vitae nisl. In a rhoncus arcu. Cras eget cursus diam. Praesent faucibus urna sit amet elementum dignissim. Nulla sem metus, tempus in imperdiet at, tempor a arcu. </p>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

        <?php include('inc/footer.inc.php') ?>

    </div>

</div>

<?php include('inc/scripts.inc.php') ?>

</body>
</html>