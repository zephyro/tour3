<!doctype html>
<html class="no-js" lang="">

<head>
    <?php include('inc/head.inc.php') ?>
</head>

<body>

<div class="page">
    <div class="grid">

        <header class="header">
            <div class="header__wrap">

                <div class="header__top">
                    <button class="header__toggle nav_toggle">
                        <i>
                            <svg class="ico_svg" viewBox="0 0 46 30" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_toggle" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                    <a class="header__logo" href="/">
                        <img src="img/logo.svg" class="img_fluid" about="">
                    </a>
                    <div class="header__content">

                        <nav class="nav">
                            <div class="nav__wrap">
                                <ul class="nav__content">
                                    <li><a href="#"><span>Правила</span></a></li>
                                    <li class="dropdown">
                                        <a href="#">
                                            <span>Условия</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                        <ul>
                                            <li><a href="#"><span>Статус</span></a></li>
                                            <li><a href="#"><span>Принятие решения</span></a></li>
                                            <li><a href="#"><span>Еще раздел</span></a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#"><span>Поддержка</span></a></li>
                                    <li><a href="#"><span>Карта сайта</span></a></li>
                                    <li><a href="#"><span>Доверие и безопасность</span></a></li>
                                    <li class="dropdown">
                                        <a href="#">
                                            <span>Поддержка</span>
                                            <i>
                                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                                </svg>
                                            </i>
                                        </a>
                                        <ul>
                                            <li><a href="#"><span>Статус</span></a></li>
                                            <li><a href="#"><span>Принятие решения</span></a></li>
                                            <li><a href="#"><span>Еще раздел</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                    </div>
                    <button class="header__nav second_toggle alert_one alert_two alert_three">
                        <i class="header__nav_base">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <i class="header__nav_one">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second_one" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <i class="header__nav_two">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second_two" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                        <i class="header__nav_three">
                            <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                <use xlink:href="img/sprite_icons.svg#icon__nav_second_three" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                            </svg>
                        </i>
                    </button>
                </div>

                <div class="header-bar">
                    <div class="header-bar__content">
                        <nav class="header-bar__menu">
                            <a class="header-bar__button" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_map" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <a class="header-bar__button active" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_list" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <a class="header-bar__button" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_user" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                            <a class="header-bar__button" href="#">
                                <i>
                                    <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                        <use xlink:href="img/sprite_icons.svg#icon__bar_doc" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                    </svg>
                                </i>
                            </a>
                        </nav>
                    </div>
                    <div class="header-bar__search">
                        <button class="header-bar__search_toggle">
                            <i>
                                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__search" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </button>
                        <div class="header-bar__search_content">
                            <div class="header-bar__search_arrow">
                                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__arrow_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </div>
                            <div class="header-bar__search_input">
                                <input type="text" name="search" value="" placeholder="Введите слово поиска">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </header>

        <?php include('inc/nav.inc.php') ?>

        <div class="page-top">
            <a href="#">
                <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__arrow_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </a>
            <span>Список проходов между точками</span>
        </div>

        <div class="main main_single">

            <div class="main_wrap">
                <div class="point mb_40">
                    <div class="point__header">Все дни</div>

                    <div class="point__group active">
                        <div class="point__group_title">
                            <span>1 день</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="point__group_content" style="display: block;">
                            <ul class="point-list">
                                <li>
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">1 Точка</div>
                                        <div class="point-list__header_info">Название первой точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">Пешком</a></div>
                                        <div class="point-list__item_text">15 минут, после мельницы, чуть правее</div>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">2 Точка</div>
                                        <div class="point-list__header_info">Название второй точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">На машине</a></div>
                                    </div>
                                    <div class="point-list__item active">
                                        <div class="point-list__item_title"><a href="#">На лодке</a></div>
                                        <div class="point-list__item_text">Лодочная станция - перерыв 13:00-14:00, закрытие 18:00</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">3 Точка</div>
                                        <div class="point-list__header_info">Название третьей точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">Пешком</a></div>
                                        <div class="point-list__item_text">15 минут, после мельницы, чуть правее</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="point__group">
                        <div class="point__group_title">
                            <span>2 день</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="point__group_content">
                            <ul class="point-list">
                                <li>
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">1 Точка</div>
                                        <div class="point-list__header_info">Название первой точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">Пешком</a></div>
                                        <div class="point-list__item_text">15 минут, после мельницы, чуть правее</div>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">2 Точка</div>
                                        <div class="point-list__header_info">Название второй точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">На машине</a></div>
                                    </div>
                                    <div class="point-list__item active">
                                        <div class="point-list__item_title"><a href="#">На лодке</a></div>
                                        <div class="point-list__item_text">Лодочная станция - перерыв 13:00-14:00, закрытие 18:00</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">3 Точка</div>
                                        <div class="point-list__header_info">Название третьей точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">Пешком</a></div>
                                        <div class="point-list__item_text">15 минут, после мельницы, чуть правее</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="point__group">
                        <div class="point__group_title">
                            <span>3 день</span>
                            <i>
                                <svg class="ico_svg" viewBox="0 0 11.288 19.324" xmlns="http://www.w3.org/2000/svg">
                                    <use xlink:href="img/sprite_icons.svg#icon__angle_left" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                </svg>
                            </i>
                        </div>
                        <div class="point__group_content">
                            <ul class="point-list">
                                <li>
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">1 Точка</div>
                                        <div class="point-list__header_info">Название первой точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">Пешком</a></div>
                                        <div class="point-list__item_text">15 минут, после мельницы, чуть правее</div>
                                    </div>
                                </li>
                                <li class="active">
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">2 Точка</div>
                                        <div class="point-list__header_info">Название второй точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">На машине</a></div>
                                    </div>
                                    <div class="point-list__item active">
                                        <div class="point-list__item_title"><a href="#">На лодке</a></div>
                                        <div class="point-list__item_text">Лодочная станция - перерыв 13:00-14:00, закрытие 18:00</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="point-list__icon">
                                        <svg class="ico_svg" viewBox="0 0 46 42.201" xmlns="http://www.w3.org/2000/svg">
                                            <use xlink:href="img/sprite_icons.svg#icon__location" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                                        </svg>
                                    </div>
                                    <div class="point-list__header">
                                        <div class="point-list__header_title">3 Точка</div>
                                        <div class="point-list__header_info">Название третьей точки 2 дня</div>
                                    </div>
                                    <div class="point-list__item">
                                        <div class="point-list__item_title"><a href="#">Пешком</a></div>
                                        <div class="point-list__item_text">15 минут, после мельницы, чуть правее</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
                <div class="text_center">
                    <a href="#" class="btn btn_border btn_fix">Редактировать тур</a>
                </div>
            </div>

        </div>

        <?php include('inc/footer.inc.php') ?>

    </div>

</div>

<?php include('inc/scripts.inc.php') ?>

</body>
</html>