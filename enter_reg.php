<!doctype html>
<html class="no-js" lang="">

<head>
    <?php include('inc/head.inc.php') ?>
</head>

<body>

<div class="page">

    <div class="grid">

        <?php include('inc/header.inc.php') ?>

        <?php include('inc/nav.inc.php') ?>

        <section class="main">

        </section>

        <?php include('inc/footer.inc.php') ?>

    </div>
</div>

<div class="auth open" id="auth">
    <div class="auth__wrap">
        <div class="auth__box">
            <div class="auth__logo">
                <svg class="ico_svg" viewBox="0 0 100 60" xmlns="http://www.w3.org/2000/svg">
                    <use xlink:href="img/sprite_icons.svg#icon__eye" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                </svg>
            </div>

            <div class="auth__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec at molestie nisl. Vivamus pulvinar enim nec odio placerat fringilla. Nam dapibus ullamcorper erat id dictum. Ut dapibus venenatis nisi a accumsan. </div>

            <div class="mb_10">
                <div class="text_center">
                    <a href="#" class="btn btn_orange btn_fix">Зарегистрироваться</a>
                </div>
            </div>
            <div class="auth__subtitle">или</div>
            <div class="social social_auth social_center">
                <a href="#" class="social__item">
                    <svg class="ico_svg" viewBox="0 0 47.361 47.361" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#ic__fb" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </a>
                <a href="#" class="social__item">
                    <svg class="ico_svg" viewBox="0 0 47.361 47.361" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#ic__g" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </a>
                <a href="#" class="social__item">
                    <svg class="ico_svg" viewBox="0 0 47.361 47.361" xmlns="http://www.w3.org/2000/svg">
                        <use xlink:href="img/sprite_icons.svg#ic__tw" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="auth__btn">
            <div class="auth__btn_item">
                <button type="button" class="btn_icon auth_close">
                    <i>
                        <svg class="ico_svg" viewBox="0 0 28.011 28.011" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__close" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <span>Закрыть окно</span>
                </button>
            </div>
            <div class="auth__btn_item">
                <button type="button" class="btn_icon auth_close">
                    <i>
                        <svg class="ico_svg" viewBox="0 0 35.417 28.011" xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="img/sprite_icons.svg#icon__check" xmlns:xlink="http://www.w3.org/1999/xlink"></use>
                        </svg>
                    </i>
                    <span>Больше не показывать</span>
                </button>
            </div>
        </div>
        <div class="text_center">

        </div>
    </div>
</div>

<?php include('inc/scripts.inc.php') ?>

</body>
</html>

